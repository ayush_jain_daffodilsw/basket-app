import React from 'react';
import './footer.css';

const Footer = (props) => {
	const footer_headline = "All, Pending, Purchased";

	return(
		<div className="footer">
	      	<p>{footer_headline}</p>
	    </div>
	)
}

export default Footer;