import React from 'react';
import './header.css';
import logo from '../../assets/images/basket_icon.png';

const Header = (props) => {
	return(
		<div className="header">
	      	<img src={logo} className="basket-logo" alt="logo" />
	      	<h2>Hello, Basket!</h2>
	    </div>
	);
}

export default Header;