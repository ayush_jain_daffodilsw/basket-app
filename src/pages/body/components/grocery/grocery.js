import React from 'react';
import { connect } from 'react-redux';
import * as actionTypes from '../../../../redux/actions/actionType'
import './grocery.css';

class Grocery extends React.Component {
	
	//data = ['Strawberry', 'Blueberry', 'Orange', 'Banana', 'Apple', 'Carrot', 'Celery', 'Mushroom', 'Bread', 'Pasta', 'Rice', 'Fish', 'Pork'];
	data = this.props.groceries;

	render() {
		return(
			<div className="grocery-section">
				<header>
					<h3>
						<i className="fa fa-leaf" aria-hidden="true"></i>
						Groceries
					</h3>
				</header>
		  		<ul className="Groceries">
		  		{
	            	this.data.map((numList,i) =>(
	                   	<li style={{color: 'black'}} key={i} onClick={() => this.props.onGroceryAdded(numList)}>{numList}</li>
	                ))
	           	}
	           	</ul>
           	</div>
		);
	}
}

const mapStateToProps = state => {
	return {
		groceries: state.groceries
	}
}

const mapDispatchToProps = dispatch => {
	return {
		onGroceryAdded: (groName) => dispatch({type: actionTypes.ADD_GROCERY, groceryName: groName}),
		getAllGroceries: () => dispatch({type: actionTypes.GET_GROCERY_LIST})
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(Grocery);