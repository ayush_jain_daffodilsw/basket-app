import React from 'react';

import './searchbox.css';

class SearchBox extends React.Component {
	render() {
		return(
			<div className="container">
			    <div className="row">
			    	<div className="col-lg-3">
			    	</div>
			        <div className="col-lg-6">
			            <div className="input-group search-bar">
			                <input type="text" className="form-control" placeholder="Search for grocery(Under Development Process)..." />
			            </div>
			        </div>
			        <div className="col-lg-3">
			    	</div>
			    </div>
			</div>
		);
	}
}

export default SearchBox;