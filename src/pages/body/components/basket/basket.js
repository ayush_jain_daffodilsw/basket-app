import React from 'react';
import { connect } from 'react-redux';
import * as actionTypes from '../../../../redux/actions/actionType'
import './basket.css';


class Basket extends React.Component {
	
	//data = ['Strawberry', 'Blueberry', 'Orange', 'Banana', 'Apple', 'Carrot', 'Celery', 'Mushroom'];
	data = this.props.groceries;
	basketData = this.props.basket;

	render() {
		return(
			<div>
				<header>
					<h3><i className="fa fa-shopping-basket" aria-hidden="true"></i>Basket</h3>
					{
						this.props.basketCounter !== 0 ?		
							<h3 className="clearBasket"><i className="fa fa-trash" aria-hidden="true" onClick={() => this.props.onRemoveAllGrocery()} ></i> </h3>
						:
						''
					}
				</header>
		  		<ul className="Basket">
		  		{
		  			this.props.basketCounter > 0 ?
		                Object.keys(this.props.basket).map((numList, i) => (
		                	this.props.basket[numList] !== 0 ? 
						    <li style={{color: 'black'}} key={i} onClick={() => this.props.onGroceryRemoved(numList)}>
						    {this.props.basket[numList]+' '+numList}</li> : '' 
						))
					: 
					<li style={{color: 'black'}}>Your basket is empty!</li>	
	           	}
	           	</ul>
           	</div>
		);
	}
}

const mapStateToProps = state => {
	return {
		groceries: state.groceries,
		basket: state.basket.basket,
		basketCounter: state.basket.basketCounter
	}
}

const mapDispatchToProps = dispatch => {
	return {
		onGroceryAdded: (groName) => dispatch({type: actionTypes.ADD_GROCERY, groceryName: groName}),
		onGroceryRemoved: (groName) => dispatch({type: actionTypes.REMOVE_GROCERY, groceryName: groName}),
		onRemoveAllGrocery: () => dispatch({type: actionTypes.REMOVE_ALL_GROCERY}),
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(Basket);