import React from 'react';

import SearchBox from './components/searchbox/';
import Grocery from './components/grocery/';
import Basket from './components/basket/';

import './body.css';

const Body = (props) => {
	return(
		<>
			<SearchBox />
			<main>
				<Grocery />
				<Basket />
			</main>

		</>
	);
}

export default Body;