import React from 'react';

import Footer from '../../component/footer/';
import Header from '../../component/header/';
import Body from '../body/';

class Home extends React.Component {

	render(){
		return(
			<>
				<Header/>
			    	<Body/>
			    <Footer/>
			</>
		);
	}
}

export default Home;